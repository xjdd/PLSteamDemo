package com.example.plstreamplayer

import android.app.Activity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*


class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn.setOnClickListener {
            player.setPath("http://yunquan.liveplay.jzzhibo.com/live/3876_370191.flv")
        }
    }

    override fun onResume() {
        super.onResume()
        player.onPauseResume()
    }

    override fun onPause() {
        super.onPause()
        player.onPausePlay()
    }

    override fun onDestroy() {
        super.onDestroy()
        player.onDestroyPlay()
    }
}
