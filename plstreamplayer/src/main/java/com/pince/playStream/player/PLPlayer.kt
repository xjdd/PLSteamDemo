package com.pince.playStream.player

import android.annotation.SuppressLint
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import com.example.plstreamplayer.R

class PLPlayer : PLPlayerController, LifecycleObserver {
    //构造函数
    constructor(context: Context) : super(context)

    @SuppressLint("Recycle", "CustomViewStyleable")
    constructor(context: Context, mAttributeSet: AttributeSet?) : super(context, mAttributeSet) {
        val ta: TypedArray = context.obtainStyledAttributes(mAttributeSet, R.styleable.PLPlayer)
        val path = ta.getString(R.styleable.PLPlayer_path)
        val isNeedFirstStart = ta.getBoolean(R.styleable.PLPlayer_isNeedFirstStart, true)
        isNeedStartFirst = isNeedFirstStart
        setPath(path)
    }

    /**
     * path 播放地址
     */
    fun setPath(path: String?) {
        path?.let {
            initPlayer(it)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onPauseResume() {
        onPlayerResume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPausePlay() {
        onPlayePause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroyPlay() {
        onPlayeStop()
    }
}