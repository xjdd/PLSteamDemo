package com.example.plstreaming;


import com.pince.pushStreaming.pusher.PLStreamingInit;

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        /**
         * init must be called before any other func
         */
        PLStreamingInit.INSTANCE.initPLStreamManager(this);
    }

}
