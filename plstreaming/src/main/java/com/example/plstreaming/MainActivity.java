package com.example.plstreaming;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.pince.pushStreaming.pusher.PLStreamPusher;


public class MainActivity extends Activity {
    private PLStreamPusher plStreamPusher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        plStreamPusher = findViewById(R.id.pl);
        plStreamPusher.setPushPath("rtmp://hmlivepush.jzzhibo.com/live/53405_1G1029000?bizid=53405&txSecret=c9204a06cd16188f8fa48368a792bb0b&txTime=5EA931D1");
        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plStreamPusher.startStream();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        plStreamPusher.onResumeStream();
    }

    @Override
    protected void onPause() {
        super.onPause();
        plStreamPusher.onPauseStream();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        plStreamPusher.onDestroyStream();
    }
}
