package com.pince.pushStreaming.pusher

import android.content.Context
import com.qiniu.pili.droid.streaming.StreamingEnv

object PLStreamingInit {
    /**
     * 必须在调用任何其他函数之前调用init
     */
    fun initPLStreamManager(context:  Context) {
        StreamingEnv.init(context)
    }
}