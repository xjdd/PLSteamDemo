package com.pince.pushStreaming.pusher

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.util.AttributeSet

class PLStreamPusher : PLStreamController, LifecycleObserver {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResumeStream() {
        super.onResume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPauseStream() {
        super.onPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroyStream() {
        super.onDestroy()
    }
}